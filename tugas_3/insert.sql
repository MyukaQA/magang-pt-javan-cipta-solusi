create database sdm;
create table departemen(
    id int primary key auto_increment,
    nama varchar(40) not null
);
create table karyawan(
    id int primary key auto_increment,
    nama varchar(40) not null,
    jenis_kelamin enum('L','P') not null,
    status varchar(20) not null,
    tanggal_lahir date not null,
    tanggal_masuk date not null,
    departemen_id int signed not null ,
    constraint departemen_fk foreign key (departemen_id)
        references departemen (id)
);

insert into departemen (nama)
values ('Manajemen'),
        ('pengembangan Bisnis'),
        ('Teknisi'),
        ('Analis');

insert into karyawan (nama, jenis_kelamin, status, tanggal_lahir, tanggal_masuk, departemen_id)
values
       ('Rizki Saputra', 'L', 'Menikah', '1980-10-11', '2011-1-1', 1),
       ('Farhan Reza', 'L', 'Belum', '1989-11-1', '2011-1-1', 1),
       ('Riyando Adi', 'L', 'Menikah', '1977-1-25', '2011-1-1', 1),
       ('Diego Manuel', 'L', 'Menikah', '1983-2-22', '2012-9-4', 2),
       ('Satya Laksana', 'L', 'Menikah', '1981-1-12', '2011-3-19', 2),
       ('Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-6-15', 2),
       ('Putri Persada', 'P', 'Menikah', '1988-1-30', '2013-4-14', 2),
       ('Alma Safira', 'P', 'Menikah', '1991-5-18', '2013-9-28', 3),
       ('Hagi Hafiz', 'L', 'Belum', '1995-9-19', '2015-3-9', 3),
       ('Abi Isyawara', 'L', 'Belum', '1991-6-3', '2012-1-22', 3),
       ('Maman Kresna', 'L', 'Belum', '1993-8-21', '2012-9-15', 3),
       ('Nadia Aulia', 'P', 'Belum', '1989-10-7', '2012-5-7', 4),
       ('Mutiara Rezki', 'P', 'Menikah', '1988-4-23', '2013-5-21', 4),
       ('Dani Setiawan', 'L', 'Belum', '1986-2-11', '2014-11-30', 4),
       ('Rizki Saputra', 'L', 'Belum', '1995-10-23', '2015-12-3', 4);
