# create database sdm;
create table departemen(
    id int primary key auto_increment,
    nama varchar(40) not null
);
create table direktur(
    id int primary key auto_increment,
    nama varchar(40) not null
);
create table manajer(
    id int primary key auto_increment,
    nama varchar(40) not null ,
    direktur_id int signed null ,
    constraint direktur_fk foreign key (direktur_id)
        references direktur (id)
);
create table staff(
    id int primary key auto_increment,
    nama varchar(40) not null,
    jenis_kelamin enum('L','P') not null,
    status varchar(20) not null,
    tanggal_lahir date not null,
    tanggal_masuk date not null,
    departemen_id int signed not null ,
    manajer_id int signed null ,
    constraint departemen_fk foreign key (departemen_id)
        references departemen (id),
    constraint manajer_fk foreign key (manajer_id)
        references manajer (id)
);

insert into departemen (nama)
values ('Manajemen'),
        ('pengembangan Bisnis'),
        ('Teknisi'),
        ('Analis');

insert into direktur (nama)
values ('Rizki Saputra');

insert into manajer (nama, direktur_id)
values ('Farhan Reza',1),
        ('Riyando Adi',1);

insert into staff (nama, jenis_kelamin, status, tanggal_lahir, tanggal_masuk, departemen_id, manajer_id)
values
       ('Diego Manuel', 'L', 'Menikah', '1983-2-22', '2012-9-4', 2, 1),
       ('Satya Laksana', 'L', 'Menikah', '1981-1-12', '2011-3-19', 2, 1),
       ('Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-6-15', 2, 1),
       ('Putri Persada', 'P', 'Menikah', '1988-1-30', '2013-4-14', 2, 1),
       ('Alma Safira', 'P', 'Menikah', '1991-5-18', '2013-9-28', 3, 1),
       ('Hagi Hafiz', 'L', 'Belum', '1995-9-19', '2015-3-9', 3, 1),
       ('Abi Isyawara', 'L', 'Belum', '1991-6-3', '2012-1-22', 3, 1),
       ('Maman Kresna', 'L', 'Belum', '1993-8-21', '2012-9-15', 3, 1),
       ('Nadia Aulia', 'P', 'Belum', '1989-10-7', '2012-5-7', 4, 2),
       ('Mutiara Rezki', 'P', 'Menikah', '1988-4-23', '2013-5-21', 4, 2),
       ('Dani Setiawan', 'L', 'Belum', '1986-2-11', '2014-11-30', 4, 2),
       ('Rizki Saputra', 'L', 'Belum', '1995-10-23', '2015-12-3', 4, 2);

select s.nama, d.nama, m.nama
from staff s
join departemen d on d.id = s.departemen_id
join manajer m on m.id = s.manajer_id;

