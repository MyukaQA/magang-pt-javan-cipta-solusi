# create database sdm;
create table departemen(
    id int primary key auto_increment,
    nama varchar(40) not null
);
create table company(
    id int primary key auto_increment,
    nama varchar(40),
    alamat text
);
create table employee(
    id int primary key auto_increment,
    nama varchar(40),
    atasan_id int null ,
    company_id int signed null ,
    constraint company_fk foreign key (company_id)
        references company (id)
);

insert into departemen (nama)
values ('Manajemen'),
        ('pengembangan Bisnis'),
        ('Teknisi'),
        ('Analis');

insert into company (nama,alamat)
values ('PT Javan','Sleman'),
        ('PT Dicoding','Bandung');

insert into employee (nama,atasan_id,company_id)
values ('Pak Budi',null,1),
        ('Pak Tono',1,1),
        ('Pak Totok',1,1),
        ('Bu Sinta',2,1),
        ('Bu Novi',3,1),
        ('Andre',4,1),
        ('Dono',4,1),
        ('Ismir',5,1),
        ('Anto',5,1);

select e.nama as CEO
from employee e
where e.atasan_id is null;

select e.nama as Direktur
from employee e
where atasan_id = (select e.id
from employee e
where e.atasan_id is null
);

select e.nama as Manager
from employee e
where e.atasan_id in (select e.id
from employee e
where atasan_id = (select e.id
from employee e
where e.atasan_id is null
));

select e.nama as Staff
from employee e
where e.atasan_id in (select e.id
from employee e
where e.atasan_id in (select e.id
from employee e
where atasan_id = (select e.id
from employee e
where e.atasan_id is null
)));

select e.nama as atasan, count(b.nama) as staff
from employee e, employee b
where e.id = b.atasan_id
group by e.nama, e.id
order by e.id asc;
