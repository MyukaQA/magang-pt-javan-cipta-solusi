<?php

namespace Database\Seeders;

use App\Models\MataKuliah;
use Illuminate\Database\Seeder;

class MataKuliahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MataKuliah::create([
            'nama' => 'Matematika',
            'jumlah_sks' => 3,
            'kelas_id' => '1',
        ]);

        MataKuliah::create([
            'nama' => 'Bahasa Indonesia',
            'jumlah_sks' => 2,
            'kelas_id' => '2',
        ]);

        MataKuliah::create([
            'nama' => 'Bahasa Inggris',
            'jumlah_sks' => 2,
            'kelas_id' => '3',
        ]);

        MataKuliah::create([
            'nama' => 'Statistika',
            'jumlah_sks' => 2,
            'kelas_id' => '4',
        ]);

        MataKuliah::create([
            'nama' => 'Pemrograman Dasar',
            'jumlah_sks' => 3,
            'kelas_id' => '5',
        ]);

        MataKuliah::create([
            'nama' => 'Sistem Basis Data',
            'jumlah_sks' => 3,
            'kelas_id' => '6',
        ]);

        MataKuliah::create([
            'nama' => 'Administrasi Basis Data',
            'jumlah_sks' => 3,
            'kelas_id' => '7',
        ]);

        MataKuliah::create([
            'nama' => 'Administrasi Sistem',
            'jumlah_sks' => 3,
            'kelas_id' => '1',
        ]);

        MataKuliah::create([
            'nama' => 'Teknik Digital',
            'jumlah_sks' => 3,
            'kelas_id' => '2',
        ]);

        MataKuliah::create([
            'nama' => 'Teknik Digital Lanjutan',
            'jumlah_sks' => 3,
            'kelas_id' => '3',
        ]);
    }
}
