<?php

namespace Database\Seeders;

use App\Models\Kelas;
use Illuminate\Database\Seeder;

class KelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::create(['nama' => 'Gedung A - Ruang Aula A']);
        Kelas::create(['nama' => 'Gedung A - Ruang Aula B']);
        Kelas::create(['nama' => 'Gedung A - Ruang 1']);
        Kelas::create(['nama' => 'Gedung A - Ruang 2']);
        Kelas::create(['nama' => 'Gedung A - Ruang 3']);
        Kelas::create(['nama' => 'Gedung A - Ruang 4']);
        Kelas::create(['nama' => 'Gedung A - Ruang 5']);
    }
}
