<?php

use App\Http\Controllers\DosenController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MatakuliahController;
use App\Http\Controllers\RiwayatPendidikanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::prefix('dosen')->group(function () {
    Route::get('/data', [DosenController::class, 'getData'])->name('dosen.data');
    Route::get('/', [DosenController::class, 'index'])->name('dosen.index');
    Route::get('/create', [DosenController::class, 'create'])->name('dosen.create');
    Route::post('/store', [DosenController::class, 'store'])->name('dosen.store');
    Route::get('/{id}/show', [DosenController::class, 'show'])->name('dosen.show');
    Route::get('/{id}/edit', [DosenController::class, 'edit'])->name('dosen.edit');
    Route::get('/{id}/delete', [DosenController::class, 'destroy'])->name('dosen.delete');

    Route::prefix('matkul')->group(function () {
        Route::get('/{id}/data', [DosenController::class, 'getDataMatkul'])->name('dosen.matkul.data');
        Route::post('/store', [DosenController::class, 'store_matkul'])->name('dosen.matkul.store');
        Route::get('/{id}/edit', [DosenController::class, 'edit_matkul'])->name('dosen.matkul.edit');
        Route::get('/{id}/delete', [DosenController::class, 'destroy_matkul'])->name('dosen.matkul.delete');
    });
});

Route::prefix('riwayat')->group(function () {
    Route::get('/{id}/create', [RiwayatPendidikanController::class, 'create'])->name('riwayat.create');
    Route::post('/store', [RiwayatPendidikanController::class, 'store'])->name('riwayat.store');
    Route::get('/{id}/edit', [RiwayatPendidikanController::class, 'edit'])->name('riwayat.edit');
    Route::post('/{id}/update', [RiwayatPendidikanController::class, 'update'])->name('riwayat.update');
    Route::get('/delete', [RiwayatPendidikanController::class, 'destroy'])->name('riwayat.delete');
});

Route::prefix('mahasiswa')->group(function () {
    Route::get('/data', [MahasiswaController::class, 'getData'])->name('mahasiswa.data');
    Route::get('/', [MahasiswaController::class, 'index'])->name('mahasiswa.index');
    Route::get('/{id}/show', [MahasiswaController::class, 'show'])->name('mahasiswa.show');
    Route::post('/store', [MahasiswaController::class, 'store'])->name('mahasiswa.store');
    Route::get('/{id}/edit', [MahasiswaController::class, 'edit'])->name('mahasiswa.edit');
    Route::get('/{id}/delete', [MahasiswaController::class, 'destroy'])->name('mahasiswa.delete');

    Route::prefix('matkul')->group(function () {
        Route::get('/{id}/data', [MahasiswaController::class, 'getDataMatkul'])->name('mahasiswa.matkul.data');
        Route::post('/store', [MahasiswaController::class, 'store_matkul'])->name('mahasiswa.matkul.store');
        Route::get('/{id}/edit', [MahasiswaController::class, 'edit_matkul'])->name('mahasiswa.matkul.edit');
        Route::get('/{id}/delete', [MahasiswaController::class, 'destroy_matkul'])->name('mahasiswa.matkul.delete');
    });
});

Route::prefix('matkul')->group(function () {
    Route::get('/data', [MatakuliahController::class, 'getData'])->name('matkul.data');
    Route::get('/', [MatakuliahController::class, 'index'])->name('matkul.index');
    Route::post('/store', [MatakuliahController::class, 'store'])->name('matkul.store');
    Route::get('/{id}/edit', [MatakuliahController::class, 'edit'])->name('matkul.edit');
    Route::get('/{id}/delete', [MatakuliahController::class, 'destroy'])->name('matkul.delete');
});

Route::prefix('kelas')->group(function () {
    Route::get('/data', [KelasController::class, 'getData'])->name('kelas.data');
    Route::get('/', [KelasController::class, 'index'])->name('kelas.index');
    Route::post('/store', [KelasController::class, 'store'])->name('kelas.store');
    Route::get('/{id}/edit', [KelasController::class, 'edit'])->name('kelas.edit');
    Route::get('/{id}/delete', [KelasController::class, 'destroy'])->name('kelas.delete');
});
