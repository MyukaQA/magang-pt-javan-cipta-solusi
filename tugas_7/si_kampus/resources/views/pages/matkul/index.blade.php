@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Mata Kuliah</h4>
        </div>
        <div class="card-body">
          <form action="">
            <div class="form-group">
              <label>Nama Mata Kuliah</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </div>
            <div class="form-group">
              <label>Jumlah SKS</label>
              <input type="number" name="jumlah_sks" id="jumlah_sks" class="form-control">
            </div>
            <div class="form-group">
              <label>Kelas</label>
              <select name="kelas_id" id="kelas_id" class="form-control">
                <option value="">-- Pilih Kelas --</option>
                @foreach ($kelas as $k)
                <option value="{{$k->id}}">{{$k->nama}}</option>
                @endforeach
              </select>
            </div>
            <button class="proses btn btn-primary" id="proses">Simpan</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header">
          <h4>Data Mata Kuliah</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="matkul-json">
              <thead>
                <th>Nama Mata Kuliah</th>
                <th>Jumlah SKS</th>
                <th>Kelas</th>
                <th>Aksi</th>
              </thead>
  
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
<script>
  var flag = "1";
  var table = $('#matkul-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("matkul.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#matkul-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "nama"},
      {"data" : "jumlah_sks"},
      {"data" : "kelas_id"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("matkul.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'nama' : $('#nama').val(),
        'jumlah_sks' : $('#jumlah_sks').val(),
        'kelas_id' : $('#kelas_id').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data matkul',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data matkul',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('matkul') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#nama').val(res.nama);
        $('#jumlah_sks').val(res.jumlah_sks);
        $('#kelas_id').val(res.kelas_id);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('matkul') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data matkul',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data matkul',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#nama').val('');
    $('#jumlah_sks').val('');
    $('#kelas_id').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>
@endsection