@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Dosen</h4>
        </div>
        <div class="card-body">
          <form action="" >
            {{ csrf_field() }}
            <div class="form-group">
              <label>Nama Dosen</label>
              <input type="text" name="nama" id="nama" class="form-control" required>
            </div>
            <div class="form-group">
              <label>NIP</label>
              <input type="text" name="nip" id="nip" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Gelar</label>
              <input type="text" name="gelar" id="gelar" class="form-control" required>
            </div>
            <button id="proses" class="proses btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header">
          <h4>Dosen</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="dosen-json">
              <thead>
                <th>Nama dosen</th>
                <th>NIP</th>
                <th>Gelar</th>
                <th>Aksi</th>
              </thead>
  
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
<script>
  var flag = "1";
  var table = $('#dosen-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("dosen.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#dosen-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "nama"},
      {"data" : "nip"},
      {"data" : "gelar"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("dosen.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'nama' : $('#nama').val(),
        'nip' : $('#nip').val(),
        'gelar' : $('#gelar').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data dosen',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data dosen',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('dosen') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#nama').val(res.nama);
        $('#nip').val(res.nip);
        $('#gelar').val(res.gelar);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('dosen') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data dosen',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data dosen',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#nama').val('');
    $('#nip').val('');
    $('#gelar').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>
@endsection