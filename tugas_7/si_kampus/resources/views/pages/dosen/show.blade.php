@extends('layouts.app')
@section('title')
  Dosen {{$dosen->nama}}
@endsection
@section('content')
  <div class="row">
    <div class="col-lg-4">
      <img src="{{asset('img/avatar/avatar.png')}}" alt="" class="img-fluid">
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header d-flex justify-content-between">
          <h2>Dosen</h2>
          <div>
            <a href="{{route('riwayat.create', \Crypt::encrypt($dosen->id))}}" class="btn btn-primary">Tambah Riwayat Pendidikan</a>
          </div>
        </div>
        <div class="card-body">
          <table>
            <tr>
              <td style="padding-right: 20px"><h5>Nama</h5></td>
              <td style="padding-right: 30px"><h5>:</h5></td>
              <td><h5>{{$dosen->nama}}</h5></td>
            </tr>
            <tr>
              <td><h5>NIP</h5></td>
              <td><h5>:</h5></td>
              <td><h5>{{$dosen->nip}}</h5></td>
            </tr>
            <tr>
              <td><h5>Gelar</h5></td>
              <td><h5>:</h5></td>
              <td><h5>{{$dosen->gelar}}</h5></td>
            </tr>  
          </table>
          <br>
          <h4>Daftar Riwayat Pendidikan</h4>
          @if ($dosen->riwayat_pendidikan->count() < 1)
            <p>Belum ada riwayat pendidikan</p>
          @else
          <table class="table">
            <thead>
              <tr>
                <th>Strata</th>
                <th>Jurusan</th>
                <th>Sekolah</th>
                <th>Tahun Ajaran</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($dosen->riwayat_pendidikan()->orderBy('created_at','desc')->get() as $r)
              <tr>
                <td>{{$r->strata}}</td>
                <td>{{$r->jurusan}}</td>
                <td>{{$r->sekolah}}</td>
                <td>{{$r->tahun_mulai}} - {{$r->tahun_selesai}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="row py-4">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Mata Kuliah</h4>
        </div>
        <div class="card-body">
          <form action="">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="hidden" name="dosen_id" id="dosen_id" value="{{$dosen->id}}">
              <select name="mata_kuliah_id" id="mata_kuliah_id" class="form-control">
                <option value="">-- Pilih Mata Kuliah --</option>
                @foreach ($matkul as $m)
                  <option value="{{$m->id}}">{{$m->nama}} ({{$m->jumlah_sks}})</option>
                @endforeach
              </select>
            </div>
            <button class="proses btn btn-primary" id="proses">Simpan</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="dosenmatkul-json">
              <thead>
                <th>Nama Mata Kuliah</th>
                <th>Jumlah SKS</th>
                <th>Aksi</th>
              </thead>
  
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
<script>
  var flag = "1";
  var table = $('#dosenmatkul-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("dosen.matkul.data", $dosen->id) }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#dosenmatkul-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "nama"},
      {"data" : "jumlah_sks"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("dosen.matkul.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'dosen_id' : $('#dosen_id').val(),
        'mata_kuliah_id' : $('#mata_kuliah_id').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data Mata Kuliah',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data Mata Kuliah',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('dosen/matkul') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#mata_kuliah_id').val(res.matkul);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    console.log(id);
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('dosen/matkul') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data Mata Kuliah',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data Mata Kuliah',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#mata_kuliah_id').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>
@endsection