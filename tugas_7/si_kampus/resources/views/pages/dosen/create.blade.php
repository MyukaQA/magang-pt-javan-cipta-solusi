@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Dosen</h4>
        </div>
        <div class="card-body">
          <form action="{{route('dosen.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-row">
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>Nama Dosen</label>
                <input type="text" name="nama" class="form-control" required>
              </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>NIP</label>
                <input type="text" name="nip" class="form-control" required>
              </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>Gelar</label>
                <input type="text" name="gelar" class="form-control" required>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection