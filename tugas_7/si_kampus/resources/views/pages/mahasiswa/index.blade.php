@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Mahasiswa</h4>
        </div>
        <div class="card-body">
          <form action="">
            <div class="form-group">
              <label>Nama Mahasiswa</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </div>
            <div class="form-group">
              <label>NIM</label>
              <input type="text" name="nim" id="nim" class="form-control">
            </div>
            <div class="form-group">
              <label>Jenis Kelamin</label>
              <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                <option value="lk">Laki-Laki</option>
                <option value="pr">Perempuan</option>
              </select>
            </div>
            <div class="form-group">
              <label>Tempat Lahir</label>
              <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
            </div>
            <div class="form-group">
              <label>Tanggal Lahir</label>
              <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control">
            </div>
            <button class="proses btn btn-primary" id="proses">Simpan</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header">
          <h4>mahasiswa</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="mahasiswa-json">
              <thead>
                <th>Nama Mahasiswa</th>
                <th>NIM</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Tanggal Lahir</th>
                <th>Aksi</th>
              </thead>
  
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
<script>
  var flag = "1";
  var table = $('#mahasiswa-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("mahasiswa.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#mahasiswa-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "nama"},
      {"data" : "nim"},
      {"data" : "jenis_kelamin"},
      {"data" : "ttl"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("mahasiswa.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'nama' : $('#nama').val(),
        'nim' : $('#nim').val(),
        'jenis_kelamin' : $('#jenis_kelamin').val(),
        'tempat_lahir' : $('#tempat_lahir').val(),
        'tanggal_lahir' : $('#tanggal_lahir').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data mahasiswa',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data mahasiswa',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('mahasiswa') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#nama').val(res.nama);
        $('#nim').val(res.nim);
        $('#jenis_kelamin').val(res.jenis_kelamin);
        $('#tempat_lahir').val(res.tempat_lahir);
        $('#tanggal_lahir').val(res.tanggal_lahir);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('mahasiswa') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data mahasiswa',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data mahasiswa',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#nama').val('');
    $('#nim').val('');
    $('#jenis_kelamin').val('');
    $('#tempat_lahir').val('');
    $('#tanggal_lahir').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>
@endsection