@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Riwayat Pendidikan Dosen</h4>
        </div>
        <div class="card-body">
          <form action="{{route('riwayat.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-row">
              <input type="hidden" name="dosen_id" value="{{$id}}">
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>Strata</label>
                <input type="text" name="strata" class="form-control" required>
              </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>Jurusan</label>
                <input type="text" name="jurusan" class="form-control" required>
              </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                <label>Sekolah</label>
                <input type="text" name="sekolah" class="form-control" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-12">
                <label>Tahun Mulai</label>
                <input type="text" name="tahun_mulai" class="form-control" required>
              </div>
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-12">
                <label>Tahun Selesai</label>
                <input type="text" name="tahun_selesai" class="form-control" required>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection