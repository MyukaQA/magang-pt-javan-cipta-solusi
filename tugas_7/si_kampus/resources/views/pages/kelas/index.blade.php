@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h4>Tambah Kelas</h4>
        </div>
        <div class="card-body">
          <form action="">
            <div class="form-group">
              <label>Nama Kelas</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </div>
            <button class="proses btn btn-primary" id="proses">Simpan</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header">
          <h4>Data Kelas</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="kelas-json">
              <thead>
                <th>Nama Kelas</th>
                <th>Aksi</th>
              </thead>
  
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
<script>
  var flag = "1";
  var table = $('#kelas-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("kelas.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#kelas-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "nama"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("kelas.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'nama' : $('#nama').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data kelas',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data kelas',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('kelas') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#nama').val(res.nama);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('kelas') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data kelas',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data kelas',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#nama').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>
@endsection