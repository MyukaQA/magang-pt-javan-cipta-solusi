<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KelasController extends Controller
{
    public function getData(Request $request){
        $pen = Kelas::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.kelas.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Kelas::where('id',$request->idedit)->update([
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Kelas::create([
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prov = Kelas::find($id);

        return response()->json($prov);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prov = Kelas::find($id);
        
        $prov->delete();
        return response()->json(['message' => 'success']);
    }
}
