<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\MataKuliah;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MatakuliahController extends Controller
{
    public function getData(Request $request){
        $pen = MataKuliah::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->editColumn('kelas_id', function($pen) {
            return $pen->kelas->nama;
        })
        ->rawColumns(['action','kelas_id'])
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kelas::all();
        return view('pages.matkul.index',compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            MataKuliah::where('id',$request->idedit)->update([
                'nama' => $request->nama,
                'jumlah_sks' => $request->jumlah_sks,            
                'kelas_id' => $request->kelas_id,            
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            MataKuliah::create([
                'nama' => $request->nama,
                'jumlah_sks' => $request->jumlah_sks,
                'kelas_id' => $request->kelas_id,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prov = MataKuliah::find($id);

        return response()->json($prov);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prov = MataKuliah::find($id);
        
        $prov->delete();
        return response()->json(['message' => 'success']);
    }
}
