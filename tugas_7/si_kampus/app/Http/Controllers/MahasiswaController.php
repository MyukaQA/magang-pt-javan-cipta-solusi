<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\MahasiswaMataKuliah;
use App\Models\MataKuliah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

use function GuzzleHttp\Promise\all;

class MahasiswaController extends Controller
{
    public function getData(Request $request){
        $pen = Mahasiswa::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="'.route('mahasiswa.show',Crypt::encrypt($pen->id)).'" class="btn btn-info btn-sm" data-id="'.$pen->id.'">Detail</a> | ';
            $btn = $btn.'<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->addColumn('ttl', function($pen) {
            $btn = $pen->tempat_lahir.", ".$pen->tanggal_lahir;
            return $btn;
        })
        ->rawColumns(['action','ttl'])
        ->make(true);
    }

    public function getDataMatkul(Request $request, $id){
        $pen = MahasiswaMataKuliah::where('mahasiswa_id',$id)->get();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->addColumn('nama', function($pen) {
            $matkul = MataKuliah::find($pen->mata_kuliah_id);
            return $matkul->nama;
        })
        ->addColumn('jumlah_sks', function($pen) {
            $matkul = MataKuliah::find($pen->mata_kuliah_id);
            return $matkul->jumlah_sks;
        })
        ->addColumn('kelas_id', function($pen) {
            $matkul = MataKuliah::find($pen->mata_kuliah_id);
            return $matkul->kelas->nama;
        })
        ->rawColumns(['action','nama','jumlah_sks','kelas_id'])
        ->make(true);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.mahasiswa.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Mahasiswa::where('id',$request->idedit)->update([
                'nama' => $request->nama,
                'nim' => $request->nim,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Mahasiswa::create([
                'nama' => $request->nama,
                'nim' => $request->nim,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    public function store_matkul(Request $request){
        $cek = Mahasiswa::find($request->mahasiswa_id);
        $matkul = MataKuliah::find($request->mata_kuliah_id);
        if($cek->mata_kuliah->sum('jumlah_sks') + $matkul->jumlah_sks >= 24){
            return response()->json(['message' => 'sks']);
        }
        if($request->flag == '0'){

            MahasiswaMataKuliah::where('id',$request->idedit)->update([
                'mahasiswa_id' => $request->mahasiswa_id,
                'mata_kuliah_id' => $request->mata_kuliah_id,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            MahasiswaMataKuliah::create([
                'mahasiswa_id' => $request->mahasiswa_id,
                'mata_kuliah_id' => $request->mata_kuliah_id,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $mhs = Mahasiswa::find($id);
        $matkul = MataKuliah::all();

        return view('pages.mahasiswa.show',compact('mhs','matkul'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mhs = Mahasiswa::find($id);

        return response()->json($mhs);
    }

    public function edit_matkul($id)
    {
        $mhs = MahasiswaMataKuliah::find($id);

        return response()->json($mhs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mhs = Mahasiswa::find($id);
        
        $mhs->delete();
        return response()->json(['message' => 'success']);
    }

    public function destroy_matkul($id)
    {
        $mhs = MahasiswaMataKuliah::find($id);
        
        $mhs->delete();
        return response()->json(['message' => 'success']);
    }
}
