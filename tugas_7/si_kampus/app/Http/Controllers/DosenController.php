<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use App\Models\DosenMataKuliah;
use App\Models\MataKuliah;
use App\Models\RiwayatPendidikan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

class DosenController extends Controller
{
    public function getData(Request $request){
        $pen = Dosen::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="'.route('dosen.show',Crypt::encrypt($pen->id)).'" class="btn btn-info btn-sm" data-id="'.$pen->id.'">Detail</a> | ';
            $btn = $btn.'<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function getDataMatkul(Request $request, $id){
        $pen = DosenMataKuliah::where('dosen_id',$id)->get();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->addColumn('nama', function($pen) {
            $matkul = MataKuliah::find($pen->mata_kuliah_id);
            return $matkul->nama;
        })
        ->addColumn('jumlah_sks', function($pen) {
            $matkul = MataKuliah::find($pen->mata_kuliah_id);
            return $matkul->jumlah_sks;
        })
        ->rawColumns(['action','nama','jumlah_sks'])
        ->make(true);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.dosen.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Dosen::where('id',$request->idedit)->update([
                'nama' => $request->nama,
                'nip' => $request->nip,
                'gelar' => $request->gelar,            
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Dosen::create([
                'nama' => $request->nama,
                'nip' => $request->nip,
                'gelar' => $request->gelar,            
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    public function store_matkul(Request $request)
    {
        if($request->flag == '1'){

            DosenMataKuliah::create([
                'dosen_id' => $request->dosen_id,
                'mata_kuliah_id' => $request->mata_kuliah_id,
            ]);
            return response()->json(['message' => 'success']);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);

        $dosen = Dosen::find($id);
        // dd($dosen->mata_kuliah);
        $matkul = MataKuliah::all();
        return view('pages.dosen.show',compact('dosen','matkul'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen = Dosen::find($id);

        return response()->json($dosen);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::find($id);
        $dosen->delete();
        return response()->json(['message' => 'success']);
    }

    public function destroy_matkul($id)
    {
        $dosenmatkul = DosenMataKuliah::find($id);
        $dosenmatkul->delete();
        return response()->json(['message' => 'success']);
    }
}
