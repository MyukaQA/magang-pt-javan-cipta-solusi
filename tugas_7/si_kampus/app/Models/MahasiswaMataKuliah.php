<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaMataKuliah extends Model
{
    use HasFactory;

    protected $table = 'mahasiswa_mata_kuliah';
    protected $guarded = ['id'];
}
