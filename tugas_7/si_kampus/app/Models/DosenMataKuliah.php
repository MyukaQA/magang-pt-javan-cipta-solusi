<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DosenMataKuliah extends Model
{
    use HasFactory;

    protected $table = 'dosen_mata_kuliah';
    protected $guarded = ['id'];
}
