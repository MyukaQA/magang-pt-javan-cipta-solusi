<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;

    protected $table = 'dosen';
    protected $guarded = ['id'];

    public function riwayat_pendidikan(){
        return $this->hasMany(RiwayatPendidikan::class);
    }

    public function mata_kuliah(){
        return $this->belongsToMany(MataKuliah::class);
    }
}
