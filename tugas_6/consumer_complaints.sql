select year(c.`Date Received`) as tahun, month(c.`Date Received`) as Bulan,count(*) as 'Jumlah Keluhan'
from consumercomplaints c
group by extract(year_month from c.`Date Received`);

select c.`Date Received`, c.Issue ,c.Tags
from consumercomplaints c
where c.Tags like '%Older American%';

select c.Company,
       count(case when c.`Company Response to Consumer` = 'Closed' then 1 end ) as Closed,
       count(case when c.`Company Response to Consumer` = 'Closed with explanation' then 1 end ) as 'Closed with explanation',
       count(case when c.`Company Response to Consumer` = 'Closed with non-monetary relief' then 1 end ) as 'Closed with non-monetary relief'
from consumercomplaints c
group by c.Company;
