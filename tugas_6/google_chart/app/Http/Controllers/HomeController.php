<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $section5_1 = DB::table('p9-consolegames')->select(DB::raw("(sum(NA_Sales) / sum(NA_Sales + JP_Sales + EU_Sales + Other_Sales) *
        100) as persen_na"))->first();
        
        $query_section5_2 = DB::table('p9-consolegames')
                    ->select('Name as nama', 'Platform as platform', 'Year as tahun')
                    ->where('Name','!=','null')
                    ->where('Platform','!=','null')
                    ->where('Year','!=','null')
                    ->orderBy('Platform','ASC')
                    ->orderBy('Year','DESC')
                    ->limit(10)
                    ->get();
        $array_query_section5_2 = array();
        foreach($query_section5_2 as $s){
            $array_query_section5_2[] = [$s->nama, $s->platform, $s->tahun];
        }
        $section5_2 = json_encode($array_query_section5_2);

        $query_section5_3 = DB::select("select Name as nama, Publisher as publisher
        from `p9-consolegames`
        where length(substring_index(Publisher, ' ', 1)) = 4 limit 50");
        $array_query_section5_3 = array();
        foreach($query_section5_3 as $s){
            $array_query_section5_3[] = [$s->nama, $s->publisher];
        }
        $section5_3 = json_encode($array_query_section5_3);

        $query_section5_4 = DB::select("select Platform as platform, FirstRetailAvailability as date
        from `p9-consoledates`
        where month(FirstRetailAvailability) <= 12 OR (month(FirstRetailAvailability) = 12 
        AND day(FirstRetailAvailability) < 25)");
        $array_query_section5_4 = array();
        foreach($query_section5_4 as $s){
            $array_query_section5_4[] = [$s->platform, $s->date];
        }
        $section5_4 = json_encode($array_query_section5_4);

        $query_section5_5 = DB::select("select FirstRetailAvailability as first_retail, Discontinued as discontinued, datediff(Discontinued, FirstRetailAvailability) as lama
        from `p9-consoledates`
        where Discontinued is not null
        order by datediff(Discontinued, FirstRetailAvailability) asc");
        $array_query_section5_5 = array();
        foreach($query_section5_5 as $s){
            $array_query_section5_5[] = [$s->first_retail, $s->discontinued, $s->lama];
        }
        $section5_5 = json_encode($array_query_section5_5);

        $query_section5_7 = DB::select("select Platform as platform, Comment as komentar
        from `p9-consoledates`
        where Discontinued is not null AND Comment is not null");
        $array_query_section5_7 = array();
        foreach($query_section5_7 as $s){
            $array_query_section5_7[] = [$s->platform, $s->komentar];
        }
        $section5_7 = json_encode($array_query_section5_7);

        $query_section7_1 = DB::select("select o.Name as owner, p.Name as pets
        from `p9-owners` o
        join `p9-pets` p on o.OwnerID = p.OwnerID");
        $array_query_section7_1 = array();
        foreach($query_section7_1 as $s){
            $array_query_section7_1[] = [$s->owner, $s->pets];
        }
        $section7_1 = json_encode($array_query_section7_1);

        $query_section7_2 = DB::select("select p.PetID as id, p.Name as name, ph.ProcedureType as pt
        from `p9-pets` p
        join `p9-procedureshistory` ph on p.PetID = ph.PetID");
        $array_query_section7_2 = array();
        foreach($query_section7_2 as $s){
            $array_query_section7_2[] = [$s->id, $s->name, $s->pt];
        }
        $section7_2 = json_encode($array_query_section7_2);

        $query_section7_3 = DB::select("select ph.PetID as id, ph.Date as date, ph.ProcedureType as pt, pd.Description as des
        from `p9-procedureshistory` ph
        join `p9-proceduresdetails` pd on ph.ProcedureSubCode = pd.ProcedureSubCode limit 100");
        $array_query_section7_3 = array();
        foreach($query_section7_3 as $s){
            $array_query_section7_3[] = [$s->id, $s->date, $s->pt, $s->des];
        }
        $section7_3 = json_encode($array_query_section7_3);

        $query_section7_4 = DB::select("
        select ph.PetID as id, p.Name as name, ph.Date as date, ph.ProcedureType as pt, pd.Description as des
        from `p9-procedureshistory` ph
        join `p9-proceduresdetails` pd on ph.ProcedureSubCode = pd.ProcedureSubCode
        join `p9-pets` p on ph.PetID = p.PetID
        group by p.Name");
        $array_query_section7_4 = array();
        foreach($query_section7_4 as $s){
            $array_query_section7_4[] = [$s->id, $s->name, $s->date, $s->pt, $s->des];
        }
        $section7_4 = json_encode($array_query_section7_4);

        $query_section7_5 = DB::select("
        select o.Name as name, sum(pd.Price) as total_price
        from `p9-owners` o
        join `p9-pets` p on o.OwnerID = p.OwnerID
        join `p9-procedureshistory` ph on p.PetID = ph.PetID
        join `p9-proceduresdetails` pd on ph.ProcedureSubCode = pd.ProcedureSubCode
        group by o.Name");
        $array_query_section7_5 = array();
        foreach($query_section7_5 as $s){
            $array_query_section7_5[] = [$s->name, (int)$s->total_price];
        }
        $section7_5 = json_encode($array_query_section7_5);

        // dd($section7_5);
        // dd(json_encode($section5_5));
        // print_r(json_encode($data));
        return view('home',compact(
            'section5_1', 
            'section5_2',
            'section5_3',
            'section5_4',
            'section5_5',
            'section5_7',
            'section7_1',
            'section7_2',
            'section7_3',
            'section7_4',
            'section7_5',
        ));
    }
}
