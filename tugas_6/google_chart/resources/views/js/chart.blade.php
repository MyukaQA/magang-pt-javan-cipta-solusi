

<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart','table','line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data_section5_1 = google.visualization.arrayToDataTable([
      ['Persen', '%'],
      ['Persentase Global - '+{{$section5_1->persen_na}}, {{$section5_1->persen_na}}],
    ]);
    var options_section5_1 = {
      title: 'Persentase NA'
    };
    var chart = new google.visualization.PieChart(document.getElementById('section5_1'));
    chart.draw(data_section5_1, options_section5_1);

    var array_data_section5_2 = {!!$section5_2!!};
    var data_section5_2 = new google.visualization.DataTable();
        data_section5_2.addColumn('string', 'Nama');
        data_section5_2.addColumn('string', 'Platform');
        data_section5_2.addColumn('number', 'Tahun');
        data_section5_2.addRows(array_data_section5_2);
    var table = new google.visualization.Table(document.getElementById('section5_2'));
    table.draw(data_section5_2, {width: '100%', height: '100%'});

    var array_data_section5_3 = {!!$section5_3!!};
    var data_section5_3 = new google.visualization.DataTable();
        data_section5_3.addColumn('string', 'Nama');
        data_section5_3.addColumn('string', 'Publisher');
        data_section5_3.addRows(array_data_section5_3);
    var table = new google.visualization.Table(document.getElementById('section5_3'));
    table.draw(data_section5_3, {width: '100%', height: '100%'});

    var array_data_section5_4 = {!!$section5_4!!};
    var data_section5_4 = new google.visualization.DataTable();
        data_section5_4.addColumn('string', 'Platform');
        data_section5_4.addColumn('string', 'Retail Date');
        data_section5_4.addRows(array_data_section5_4);
    var table = new google.visualization.Table(document.getElementById('section5_4'));
    table.draw(data_section5_4, {width: '100%', height: '100%'});

    var array_data_section5_5 = {!!$section5_5!!};
    var data_section5_5 = new google.visualization.DataTable();
        data_section5_5.addColumn('string', 'First Retail');
        data_section5_5.addColumn('string', 'Discontinued');
        data_section5_5.addColumn('number', 'Long');
        data_section5_5.addRows(array_data_section5_5);
    var table = new google.visualization.Table(document.getElementById('section5_5'));
    table.draw(data_section5_5, {width: '100%', height: '100%'});

    var array_data_section5_7 = {!!$section5_7!!};
    var data_section5_7 = new google.visualization.DataTable();
        data_section5_7.addColumn('string', 'Platform');
        data_section5_7.addColumn('string', 'Komentar');
        data_section5_7.addRows(array_data_section5_7);
    var table = new google.visualization.Table(document.getElementById('section5_7'));
    table.draw(data_section5_7, {width: '100%', height: '100%'});

    var array_data_section7_1 = {!!$section7_1!!};
    var data_section7_1 = new google.visualization.DataTable();
        data_section7_1.addColumn('string', 'Owner');
        data_section7_1.addColumn('string', 'Pets');
        data_section7_1.addRows(array_data_section7_1);
    var table = new google.visualization.Table(document.getElementById('section7_1'));
    table.draw(data_section7_1, {width: '100%', height: '100%'});

    var array_data_section7_2 = {!!$section7_2!!};
    var data_section7_2 = new google.visualization.DataTable();
        data_section7_2.addColumn('string', 'ID Pets');
        data_section7_2.addColumn('string', 'Nama');
        data_section7_2.addColumn('string', 'Prosedure Type');
        data_section7_2.addRows(array_data_section7_2);
    var table = new google.visualization.Table(document.getElementById('section7_2'));
    table.draw(data_section7_2, {width: '100%', height: '100%'});

    var array_data_section7_3 = {!!$section7_3!!};
    var data_section7_3 = new google.visualization.DataTable();
        data_section7_3.addColumn('string', 'ID Pets');
        data_section7_3.addColumn('string', 'Date');
        data_section7_3.addColumn('string', 'Prosedure Type');
        data_section7_3.addColumn('string', 'Description');
        data_section7_3.addRows(array_data_section7_3);
    var table = new google.visualization.Table(document.getElementById('section7_3'));
    table.draw(data_section7_3, {width: '100%', height: '100%'});

    var array_data_section7_4 = {!!$section7_4!!};
    var data_section7_4 = new google.visualization.DataTable();
        data_section7_4.addColumn('string', 'ID Pets');
        data_section7_4.addColumn('string', 'Name');
        data_section7_4.addColumn('string', 'Date');
        data_section7_4.addColumn('string', 'Prosedure Type');
        data_section7_4.addColumn('string', 'Description');
        data_section7_4.addRows(array_data_section7_4);
    var table = new google.visualization.Table(document.getElementById('section7_4'));
    table.draw(data_section7_4, {width: '100%', height: '100%'});

    var array_data_section7_5 = {!!$section7_5!!};
    var data_section7_5 = new google.visualization.DataTable();
        data_section7_5.addColumn('string', 'Name');
        data_section7_5.addColumn('number', 'Total Price');
        data_section7_5.addRows(array_data_section7_5);
    var options_section7_5 = {
      chart: {
        title: 'Total Price dari beberapa orang',
      },
      width: 900,
      height: 500
    };
    var chart_section7_5 = new google.charts.Line(document.getElementById('section7_5'));
    chart_section7_5.draw(data_section7_5, google.charts.Line.convertOptions(options_section7_5));

  }

</script>