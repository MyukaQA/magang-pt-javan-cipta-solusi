@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-12 py-3">
      <div class="card">
        <div class="card-header">
          <h1>Section 5</h1>
        </div>
        <div class="card-body">
          <table class="table table-striped table-inverse">
            <thead class="thead-inverse">
              <tr>
                <th>No</th>
                <th>Chart</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                  <td scope="row">1</td>
                  <td><div id="section5_1" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">2</td>
                  <td><div id="section5_2" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">3</td>
                  <td><div id="section5_3" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">4</td>
                  <td><div id="section5_4" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">5</td>
                  <td><div id="section5_5" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">6</td>
                  <td><div id="section5_6" style="width: 100&; height: 200px;">alter table  `p9-consoledates` modify Discontinued datetime;</div></td>
                </tr>
                <tr>
                  <td scope="row">7</td>
                  <td><div id="section5_7" style="width: 100&; height: 500px;"></div></td>
                </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-lg-12 py-3">
      <div class="card">
        <div class="card-header">
          <h1>Section 7</h1>
        </div>
        <div class="card-body">
          <table class="table table-striped table-inverse">
            <thead class="thead-inverse">
              <tr>
                <th>No</th>
                <th>Chart</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                  <td scope="row">1</td>
                  <td><div id="section7_1" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">2</td>
                  <td><div id="section7_2" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">3</td>
                  <td><div id="section7_3" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">4</td>
                  <td><div id="section7_4" style="width: 100&; height: 500px;"></div></td>
                </tr>
                <tr>
                  <td scope="row">5</td>
                  <td><div id="section7_5" style="width: 100&; height: 500px;"></div></td>
                </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-12 py-3">
    <div class="card">
      <div class="card-header">
        <h1>Section 9</h1>
      </div>
      <div class="card-body">
        <table class="table table-striped table-inverse">
          <thead class="thead-inverse">
            <tr>
              <th>No</th>
              <th>Chart</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td scope="row">1</td>
                <td>
                  <div id="section9_1">Section 9 berupa perbaikan design database</div>
                  <div><img src="{{asset('img/section9_1.png')}}" alt=""></div>
                </td>
              </tr>
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
  @include('js.chart')
@endsection