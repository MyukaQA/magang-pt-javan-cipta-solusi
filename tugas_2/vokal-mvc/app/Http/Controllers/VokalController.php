<?php

namespace App\Http\Controllers;

use App\Http\Requests\VokalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VokalController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('get')) {
            $hasil = null;
            return view('welcome', compact('hasil'));
        }

        if ($request->isMethod('post')) {
            $rules = [
                'text' => ['required', 'min:3'],
            ];
            $messages = [
                'text.required'        => 'Text wajib diisi.',
                'text.min'        => 'Minimal 3 Huruf.',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
            $arr = str_split($request->text);
            $vocal = ['a', 'i', 'u', 'e', 'o'];

            $found = array_intersect($vocal, $arr);
            $count = array_count_values($arr);

            $tampung = null;

            foreach ($found as $item) {
                if ($tampung == null) {
                    $tampung = $item;
                } else {
                    $tampung .= ' dan ' . $item;
                }
            }
            $hasil = '"' . $request->text . '" = ' . count($found) . ' yaitu ' . $tampung;
            return view('welcome', compact('hasil'));
        }
    }
}
