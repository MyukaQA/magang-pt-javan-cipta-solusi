<script>
  var flag = "1";
  var table = $('#kabupaten-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("kabupaten.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#kabupaten-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "id"},
      {"data" : "provinsi_id"},
      {"data" : "nama"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("kabupaten.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'provinsi_id' : $('#provinsi_id').val(),
        'nama' : $('#nama').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data kabupaten',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data kabupaten',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('kabupaten') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#provinsi_id').val(res.provinsi_id);
        $('#nama').val(res.nama);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('kabupaten') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data kabupaten',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data kabupaten',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#provinsi_id').val('');
    $('#nama').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>