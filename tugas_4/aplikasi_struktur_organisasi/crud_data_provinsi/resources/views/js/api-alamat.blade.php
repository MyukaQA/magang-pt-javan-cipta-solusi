<script>
  $('#provinsi_id').on('change', function() {
    $.ajax({
      url: "{{ url('/api/kabupaten') }}",
      type: "GET",
      data: { kabupaten: $(this).val() },
      success: function(html){
        $('#kabupaten_id').empty()
        $('#kabupaten_id').append('<option value="">-- Pilih Kabupaten --</option>')
        $.each(html.data, function(index, item) {
          $('#kabupaten_id').append('<option value="'+item['id']+'">'+item['nama']+'</option>')
        });
        
      }
    });
  });

  $('#kabupaten_id').on('change', function() {
    $.ajax({
      url: "{{ url('/api/kecamatan') }}",
      type: "GET",
      data: { kecamatan: $(this).val() },
      success: function(html){
        $('#kecamatan_id').empty()
        $('#kecamatan_id').append('<option value="">-- Pilih Kecamatan --</option>')
        $.each(html.data, function(index, item) {
          $('#kecamatan_id').append('<option value="'+item['id']+'">'+item['nama']+'</option>')
        });
        
      }
    });
  });

  $('#kecamatan_id').on('change', function() {
    $.ajax({
      url: "{{ url('/api/desa') }}",
      type: "GET",
      data: { desa: $(this).val() },
      success: function(html){
        $('#desa_id').empty()
        $('#desa_id').append('<option value="">-- Pilih Desa --</option>')
        $.each(html.data, function(index, item) {
          $('#desa_id').append('<option value="'+item['id']+'">'+item['nama']+'</option>')
        });
        
      }
    });
  });
</script>