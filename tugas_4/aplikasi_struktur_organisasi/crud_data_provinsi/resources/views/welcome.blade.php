@extends('layouts.app')
@section('title')
Home
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <form action="">
          <div class="form-group">
            <label>Nama Provinsi</label>
            <select name="provinsi_id" id="provinsi_id" class="form-control" required>
              <option value="">-- Pilih Provinsi --</option>
              @foreach ($provinsi as $p)
                <option value="{{$p->id}}">{{$p->nama}}</option>              
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Nama Kabupaten</label>
            <select name="kabupaten_id" id="kabupaten_id" class="form-control" required>
              <option value="">-- Pilih Kabupaten --</option>

            </select>
          </div>
          <div class="form-group">
            <label>Nama Kecamatan</label>
            <select name="kecamatan_id" id="kecamatan_id" class="form-control" required>
              <option value="">-- Pilih Kecamatan --</option>

            </select>
          </div>
          <div class="form-group">
            <label>Nama Desa</label>
            <select name="desa_id" id="desa_id" class="form-control" required>
              <option value="">-- Pilih Desa --</option>

            </select>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
  @include('js.api-alamat')
@endsection