@extends('layouts.app')
@section('title')
  Data kecamatan
@endsection
@section('content')
<div class="row">
  <div class="col-lg-4">
    <div class="card">
      <div class="card-header">
        <h4>Tambah kecamatan</h4>
      </div>
      <div class="card-body">
        <form action="">
          <div class="form-group">
            <label>Nama Provinsi</label>
            <select name="provinsi_id" id="provinsi_id" class="form-control" required>
              <option value="">-- Pilih Provinsi --</option>
              @foreach ($provinsi as $p)
                <option value="{{$p->id}}">{{$p->nama}}</option>              
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Nama Kabupaten</label>
            <select name="kabupaten_id" id="kabupaten_id" class="form-control" required>
              <option value="">-- Pilih Kabupaten --</option>

            </select>
          </div>
          <div class="form-group">
            <label>Nama kecamatan</label>
            <input type="text" name="nama" id="nama" class="form-control" required>
          </div>
          <button id="proses" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-8">
    <div class="card">
      <div class="card-header justify-content-between">
        <h4>Data kecamatan</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table" id="kecamatan-json">
            <thead>
              <th>ID</th>
              <th>Nama Provinsi</th>
              <th>Nama Kabupaten</th>
              <th>Nama kecamatan</th>
              <th>Aksi</th>
            </thead>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
  @include('js.js-kecamatan')
  @include('js.api-alamat')
@endsection