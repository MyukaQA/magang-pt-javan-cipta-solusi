@extends('layouts.app')
@section('title')
  Data Provinsi
@endsection
@section('content')
<div class="row">
  <div class="col-lg-4">
    <div class="card">
      <div class="card-header">
        <h4>Tambah Provinsi</h4>
      </div>
      <div class="card-body">
        <form action="">
          <div class="form-group">
            <label>Nama Provinsi</label>
            <input type="text" name="nama" id="nama" class="form-control" required>
          </div>
          <button id="proses" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-8">
    <div class="card">
      <div class="card-header justify-content-between">
        <h4>Data Provinsi</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table" id="provinsi-json">
            <thead>
              <th>ID</th>
              <th>Nama Provinsi</th>
              <th>Aksi</th>
            </thead>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
  @include('js.js-provinsi')
@endsection