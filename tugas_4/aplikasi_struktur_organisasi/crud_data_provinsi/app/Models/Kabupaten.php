<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    use HasFactory;

    protected $table = 'kabupaten';
    protected $guarded = ['id'];

    public function provinsi(){
        return $this->belongsTo(Provinsi::class);
    }

    public function kecamatan(){
        return $this->hasMany(kecamatan::class);
    }

    public function desa(){
        return $this->hasMany(Desa::class);
    }
}
