<?php

namespace App\Http\Controllers;

use App\Models\Desa;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use Illuminate\Http\Request;

class ApiCustomController extends Controller
{
    public function get_kabupaten(Request $request){
        $id = $request->kabupaten;
        $kab = Kabupaten::where('provinsi_id',$id)->get();
        return response()->json(['status' => 'success', 'data' => $kab]);
    }

    public function get_kecamatan(Request $request){
        $id = $request->kecamatan;
        $kab = Kecamatan::where('kabupaten_id',$id)->get();
        return response()->json(['status' => 'success', 'data' => $kab]);
    }

    public function get_desa(Request $request){
        $id = $request->desa;
        $kab = Desa::where('kecamatan_id',$id)->get();
        return response()->json(['status' => 'success', 'data' => $kab]);
    }
}
