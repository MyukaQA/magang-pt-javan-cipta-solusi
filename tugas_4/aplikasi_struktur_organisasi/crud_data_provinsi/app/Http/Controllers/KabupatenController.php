<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use App\Models\Provinsi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KabupatenController extends Controller
{
    public function getData(Request $request){
        $pen = Kabupaten::all();
        return DataTables::of($pen)
        ->editColumn('provinsi_id', function($pen) {
            
            return $pen->provinsi->nama;
        })
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action','provinsi_id'])
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsi = Provinsi::orderBy('created_at','desc')->get();
        return view('pages.kabupaten.index',compact('provinsi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Kabupaten::where('id',$request->idedit)->update([
                'provinsi_id' => $request->provinsi_id,
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Kabupaten::create([
                'provinsi_id' => $request->provinsi_id,
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kab = Kabupaten::find($id);

        return response()->json($kab);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kab = Kabupaten::find($id);
        
        $kab->delete();
        return response()->json(['message' => 'success']);
    }
}
