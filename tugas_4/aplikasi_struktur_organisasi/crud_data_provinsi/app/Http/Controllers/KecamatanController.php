<?php

namespace App\Http\Controllers;

use App\Models\Kecamatan;
use App\Models\Provinsi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KecamatanController extends Controller
{
    public function getData(Request $request){
        $pen = Kecamatan::all();
        return DataTables::of($pen)
        ->editColumn('provinsi_id', function($pen) {
            
            return $pen->provinsi->nama;
        })
        ->editColumn('kabupaten_id', function($pen) {
            
            return $pen->kabupaten->nama;
        })
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action','provinsi_id','kabupaten_id'])
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsi = Provinsi::orderBy('created_at','desc')->get();
        return view('pages.kecamatan.index',compact('provinsi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Kecamatan::where('id',$request->idedit)->update([
                'provinsi_id' => $request->provinsi_id,
                'kabupaten_id' => $request->kabupaten_id,
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Kecamatan::create([
                'provinsi_id' => $request->provinsi_id,
                'kabupaten_id' => $request->kabupaten_id,
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kec = Kecamatan::find($id);

        return response()->json($kec);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kec = Kecamatan::find($id);
        
        $kec->delete();
        return response()->json(['message' => 'success']);
    }
}
