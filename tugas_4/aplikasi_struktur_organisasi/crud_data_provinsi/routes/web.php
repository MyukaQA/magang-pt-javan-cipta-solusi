<?php

use App\Http\Controllers\DesaController;
use App\Http\Controllers\KabupatenController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\ProvinsiController;
use App\Models\Provinsi;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $provinsi = Provinsi::orderBy('created_at','desc')->get();
    return view('welcome',compact('provinsi'));
})->name('home');

Route::get('/provinsi/data', [ProvinsiController::class, 'getData'])->name('provinsi.data');
Route::get('/provinsi', [ProvinsiController::class, 'index'])->name('provinsi.index');
Route::post('/provinsi/store', [ProvinsiController::class, 'store'])->name('provinsi.store');
Route::get('/provinsi/{id}/edit', [ProvinsiController::class, 'edit'])->name('provinsi.edit');
Route::get('/provinsi/{id}/delete', [ProvinsiController::class, 'destroy'])->name('provinsi.destroy');

Route::get('/kabupaten/data', [KabupatenController::class, 'getData'])->name('kabupaten.data');
Route::get('/kabupaten', [KabupatenController::class, 'index'])->name('kabupaten.index');
Route::post('/kabupaten/store', [KabupatenController::class, 'store'])->name('kabupaten.store');
Route::get('/kabupaten/{id}/edit', [KabupatenController::class, 'edit'])->name('kabupaten.edit');
Route::get('/kabupaten/{id}/delete', [KabupatenController::class, 'destroy'])->name('kabupaten.destroy');

Route::get('/kecamatan/data', [KecamatanController::class, 'getData'])->name('kecamatan.data');
Route::get('/kecamatan', [KecamatanController::class, 'index'])->name('kecamatan.index');
Route::post('/kecamatan/store', [KecamatanController::class, 'store'])->name('kecamatan.store');
Route::get('/kecamatan/{id}/edit', [KecamatanController::class, 'edit'])->name('kecamatan.edit');
Route::get('/kecamatan/{id}/delete', [KecamatanController::class, 'destroy'])->name('kecamatan.destroy');

Route::get('/desa/data', [DesaController::class, 'getData'])->name('desa.data');
Route::get('/desa', [DesaController::class, 'index'])->name('desa.index');
Route::post('/desa/store', [DesaController::class, 'store'])->name('desa.store');
Route::get('/desa/{id}/edit', [DesaController::class, 'edit'])->name('desa.edit');
Route::get('/desa/{id}/delete', [DesaController::class, 'destroy'])->name('desa.destroy');
