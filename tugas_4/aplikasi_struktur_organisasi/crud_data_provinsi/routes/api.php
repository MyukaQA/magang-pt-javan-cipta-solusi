<?php

use App\Http\Controllers\ApiCustomController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('kabupaten', [ApiCustomController::class,'get_kabupaten']);
Route::get('kecamatan', [ApiCustomController::class,'get_kecamatan']);
Route::get('desa', [ApiCustomController::class,'get_desa']);
