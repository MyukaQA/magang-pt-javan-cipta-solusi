@extends('layouts.app')
@section('title')
  Data film
@endsection
@section('content')
<div class="row">
  <div class="col-lg-4">
    <div class="card">
      <div class="card-header">
        <h4>Tambah film</h4>
      </div>
      <div class="card-body">
        <form action="">
          <div class="form-group">
            <label>Judul film</label>
            <input type="text" name="judul_film" id="judul_film" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Tanggal</label>
            <input type="date" name="tanggal" id="tanggal" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Jam Mulai Film</label>
            <input type="time" name="jam_mulai" id="jam_mulai" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Jam Berakhir Film</label>
            <input type="time" name="jam_akhir" id="jam_akhir" class="form-control" required>
          </div>
          <button id="proses" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-8">
    <div class="card">
      <div class="card-header justify-content-between">
        <h4>Data film</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table" id="film-json">
            <thead>
              <th>ID</th>
              <th>Judul film</th>
              <th>Tanggal</th>
              <th>Jam Mulai Film</th>
              <th>Jam Berakhir Film</th>
              <th>Aksi</th>
            </thead>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="text" name="idedit" id="idedit" value="0" hidden>
@endsection
@section('js')
  @include('js.film')
@endsection