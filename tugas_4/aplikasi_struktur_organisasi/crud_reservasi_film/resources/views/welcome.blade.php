@extends('layouts.app')
@section('title')
    Home
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('home.store')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Film</label>
                            <select name="film_id" id="film_id" class="form-control">
                                <option value="">-- Pilih Film --</option>
                                @foreach ($film as $f)
                                    <option value="{{$f->id}}">
                                        {{$f->judul_film}} / {{$f->tanggal}} ({{$f->jam_mulai}} - {{$f->jam_akhir}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Penonton</label>
                            <select name="penonton_id" id="penonton_id" class="form-control">
                                <option value="">-- Pilih Penonton --</option>
                                @foreach ($penonton as $p)
                                    <option value="{{$p->id}}">
                                        {{$p->nama}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th>Film</th>
                                <th>Tanggal</th>
                                <th>Jam</th>
                                <th>Penonton</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($film as $r)
                                <tr>
                                    <td>{{$r->judul_film}}</td>
                                    <td>{{$r->tanggal}}</td>
                                    <td>{{$r->jam_mulai}} - {{$r->jam_akhir}}</td>
                                    <td>
                                        <ul>
                                            @foreach ($r->penonton as $item)
                                            <li>{{$item->nama}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection