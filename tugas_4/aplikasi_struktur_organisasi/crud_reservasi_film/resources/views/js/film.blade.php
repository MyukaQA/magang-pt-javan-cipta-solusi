<script>
  var flag = "1";
  var table = $('#film-json').DataTable({
    processing: false,
    serverSide: true,
    "ordering": false,
    searching: false,
    "ajax" : {
      "url" : '{{ route("film.data") }}',
      "error" : function (jqXHR, textStatus, errorThrown) {
        $('#film-json').DataTable().clear().draw();
      }
    },
    "columns": [
      {"data" : "id"},
      {"data" : "judul_film"},
      {"data" : "tanggal"},
      {"data" : "jam_mulai"},
      {"data" : "jam_akhir"},
      {"data" : "action"},
    ]
  });

  $('#proses').click(function(e){
    e.preventDefault();
    var tipe = 'POST';
    var URL = '{{ route("film.store") }}';

    console.log($('#idedit').val());
    $.ajax({
      url : URL,
      type : tipe,
      data : {
        'idedit' : $('#idedit').val(),
        'judul_film' : $('#judul_film').val(),
        'tanggal' : $('#tanggal').val(),
        'jam_mulai' : $('#jam_mulai').val(),
        'jam_akhir' : $('#jam_akhir').val(),
        'flag' : flag,
        _token : $("meta[name='csrf-token']").attr("content")
      },
      dataType: 'JSON',
      success : function(res){
        if(res.message == 'success'){
          Swal.fire({
            title: 'Data film',
            text: 'data berhasil disimpan',
            icon: 'success', 
            timer: 5000,
          });
          clear();
        }else {
          Swal.fire({
            title: 'Data film',
            text: 'data tidak berhasil disimpan',
            icon: 'error', 
            timer: 5000,
          });
        }
      }
    });
  });

  $(document).on('click', '.edit', function (e) {
    
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
      url: "{{ url('film') }}/" + id+"/edit",
      success: function (res) {
        $('#idedit').val(res.id);
        $('#judul_film').val(res.judul_film);
        $('#tanggal').val(res.tanggal);
        $('#jam_mulai').val(res.jam_mulai);
        $('#jam_akhir').val(res.jam_akhir);
        $('#proses').text('Update');
        flag = "0";
      }         
    });
  });

  $(document).on('click', '.hapus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
      title: 'Apa Anda yakin untuk menghapusnya ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "{{ url('film') }}/" + id + "/delete",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content")
          },
          success: function (res) {
            if(res.message == 'success'){
              Swal.fire({
                title: 'Data film',
                text: 'data berhasil dihapus',
                icon: 'success', 
                timer: 5000,
              });
              clear();
            } else {
              Swal.fire({
                title: 'Data film',
                text: 'data tidak berhasil dihapus',
                icon: 'error', 
                timer: 5000,
              });
            }
          }         
        });
      }
    });
  });

  function clear(){
    flag = "1";
    $('#judul_film').val('');
    $('#tanggal').val('');
    $('#jam_mulai').val('');
    $('#jam_akhir').val('');
    $('#table').html('');
    $('#proses').text('Simpan');
    table.clear().draw();

  }
</script>