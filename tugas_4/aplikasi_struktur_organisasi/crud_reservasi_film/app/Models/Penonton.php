<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    use HasFactory;

    protected $table = 'penonton';
    protected $guarded = ['id'];

    public function film(){
        return $this->belongsToMany(Film::class);
    }
}
