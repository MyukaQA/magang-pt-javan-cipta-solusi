<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'film';
    protected $guarded = ['id'];

    public function penonton(){
        return $this->belongsToMany(Penonton::class);
    }
}
