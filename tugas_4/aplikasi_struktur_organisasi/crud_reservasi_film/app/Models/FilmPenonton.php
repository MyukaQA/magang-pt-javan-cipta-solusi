<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmPenonton extends Model
{
    use HasFactory;

    protected $table = 'film_penonton';
    protected $guarded = ['id'];
}
