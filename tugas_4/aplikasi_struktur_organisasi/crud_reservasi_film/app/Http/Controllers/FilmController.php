<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class FilmController extends Controller
{
    public function getData(Request $request){
        $pen = Film::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.film.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Film::where('id',$request->idedit)->update([
                'judul_film' => $request->judul_film,
                'tanggal' => $request->tanggal,
                'jam_mulai' => $request->jam_mulai,
                'jam_akhir' => $request->jam_akhir,
            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Film::create([
                'judul_film' => $request->judul_film,
                'tanggal' => $request->tanggal,
                'jam_mulai' => $request->jam_mulai,
                'jam_akhir' => $request->jam_akhir,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);

        return response()->json($film);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        
        $film->delete();
        return response()->json(['message' => 'success']);
    }
}
