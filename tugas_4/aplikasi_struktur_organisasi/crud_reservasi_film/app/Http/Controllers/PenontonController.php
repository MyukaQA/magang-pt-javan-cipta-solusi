<?php

namespace App\Http\Controllers;

use App\Models\Penonton;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PenontonController extends Controller
{
    public function getData(Request $request){
        $pen = Penonton::all();
        return DataTables::of($pen)
        ->addColumn('action', function($pen) {
            $btn = '<a href="#" class="edit btn btn-warning btn-sm" data-id="'.$pen->id.'">Edit</a> | ';
            $btn = $btn.'<a href="#" class="hapus btn btn-danger btn-sm" data-id="'.$pen->id.'">Hapus</a> ';

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.penonton.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->flag == '0'){

            Penonton::where('id',$request->idedit)->update([
                'nama' => $request->nama,            ]);
            return response()->json(['message' => 'success']);
            
        }else{

            Penonton::create([
                'nama' => $request->nama,
            ]);
            return response()->json(['message' => 'success']);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = Penonton::find($id);

        return response()->json($p);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Penonton::find($id);
        
        $p->delete();
        return response()->json(['message' => 'success']);
    }
}
