<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\FilmPenonton;
use App\Models\Penonton;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $film = Film::all();
        $penonton = Penonton::all();
        return view('welcome',compact('film','penonton'));
    }

    public function store(Request $request){
        FilmPenonton::create($request->all());

        return redirect()->back();
    }
}
