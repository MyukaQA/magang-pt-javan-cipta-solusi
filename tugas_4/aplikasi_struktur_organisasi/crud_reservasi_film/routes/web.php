<?php

use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PenontonController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/store', [HomeController::class, 'store'])->name('home.store');

Route::get('/film/data', [FilmController::class, 'getData'])->name('film.data');
Route::get('/film', [FilmController::class, 'index'])->name('film.index');
Route::post('/film/store', [FilmController::class, 'store'])->name('film.store');
Route::get('/film/{id}/edit', [FilmController::class, 'edit'])->name('film.edit');
Route::get('/film/{id}/delete', [FilmController::class, 'destroy'])->name('film.destroy');

Route::get('/penonton/data', [PenontonController::class, 'getData'])->name('penonton.data');
Route::get('/penonton', [PenontonController::class, 'index'])->name('penonton.index');
Route::post('/penonton/store', [PenontonController::class, 'store'])->name('penonton.store');
Route::get('/penonton/{id}/edit', [PenontonController::class, 'edit'])->name('penonton.edit');
Route::get('/penonton/{id}/delete', [PenontonController::class, 'destroy'])->name('penonton.destroy');

