<?php

namespace App\Exports;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OrganisasiExport implements FromCollection, WithHeadings, WithColumnWidths, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = DB::table('employee')
                    ->select(
                        'employee.id',
                        'employee.nama',
                        DB::raw('(
                            CASE WHEN atasan_id is null THEN "CEO"
                            WHEN atasan_id = (select id from employee where atasan_id is null) THEN "Direktur"
                            WHEN atasan_id in (select id from employee where atasan_id = (select id from employee where atasan_id is null)) THEN "Manager"
                            WHEN atasan_id in (select id from employee where atasan_id in (select id from employee where atasan_id = (select id from employee where atasan_id is null))) THEN "Staff"
                            END
                        ) as Posisi'),
                        'company.nama as Perusahaan',
                    )
                    ->join('company','employee.company_id','=','company.id')
                    ->get();
        return $query;
    }

    public function headings(): array
    {
        return [
            'id',
            'Nama',
            'Posisi',
            'Perusahaan',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 20,
            'C' => 20,
            'D' => 20,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $sheet){
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT
                    ],
                ];
                
                $sheet->sheet->getStyle('A1:D10')->applyFromArray($styleArray);
            }
        ];
    }
}
