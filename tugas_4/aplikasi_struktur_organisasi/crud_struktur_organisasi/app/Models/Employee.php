<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'employee';
    protected $guarded = ['id'];

    public $timestamps = false;

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function atasan(){
        return $this->belongsTo(self::class, 'atasan_id');
    }

    public function bawahan(){
        return $this->hasMany(self::class, 'atasan_id');
    }
}
