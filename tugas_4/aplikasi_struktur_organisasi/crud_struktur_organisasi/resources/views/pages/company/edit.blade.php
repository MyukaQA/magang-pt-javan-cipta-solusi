@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h4>Company</h4>
            </div>
            <div class="float-right">
              <a href="{{route('company.index')}}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
          <div class="card-body">
            <form action="{{route('company.update',$company->id)}}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Nama Perusahaan</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$company->nama}}">
              </div>
              <div class="form-group">
                <label>Alamat Perusahaan</label>
                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control">{{$company->alamat}}</textarea>
              </div>
              <button type="submit" class="btn btn-warning">Update</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection