@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h4>Company</h4>
            </div>
            <div class="float-right">
              <a href="{{route('export.pdf')}}" class="btn btn-danger">Download Data PDF</a>
              <a href="{{route('export.excel')}}" class="btn btn-success">Download Data Excel</a>
              <a href="{{route('company.create')}}" class="btn btn-primary">Tambah Data Company</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-dark">
                <thead>
                  <tr>
                    <td scope="col">ID</td>
                    <td scope="col">Nama</td>
                    <td scope="col">Alamat</td>
                    <td scope="col">Aksi</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($company as $c)
                    <tr>
                      <td scope="row">{{$c->id}}</td>
                      <td>{{$c->nama}}</td>
                      <td>{{$c->alamat}}</td>
                      <td>
                        <a href="{{route('company.edit', $c->id)}}" class="btn btn-warning btn-sm">Edit</a> | 
                        <a href="{{route('company.delete', $c->id)}}" onclick="return confirm('Yakin ingin dihapus ?');" class="btn btn-danger btn-sm">Hapus</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection