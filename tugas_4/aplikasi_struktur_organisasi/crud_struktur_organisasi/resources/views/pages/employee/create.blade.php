@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h4>Employee</h4>
            </div>
            <div class="float-right">
              <a href="{{route('employee.index')}}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
          <div class="card-body">
            <form action="{{route('employee.store')}}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" id="nama">
              </div>
              <div class="form-group">
                <label>Atasan</label>
                <select name="atasan_id" id="atasan_id" class="form-control">
                  <option value="">Tidak Ada</option>
                  @foreach ($atasan as $item)
                  @if ($item->bawahan->count() != 0)
                  <option value="{{$item->id}}">{{$item->nama}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Perusahaan</label>
                <select name="company_id" id="company_id" class="form-control">
                  <option value="">Tidak Ada</option>
                  @foreach ($company as $item)
                  <option value="{{$item->id}}">{{$item->nama}}</option>
                  @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection