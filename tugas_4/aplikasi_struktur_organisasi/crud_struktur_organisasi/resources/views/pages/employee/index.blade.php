@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h4>Employee</h4>
            </div>
            <div class="float-right">
              <a href="{{route('export.pdf')}}" class="btn btn-danger">Download Data PDF</a>
              <a href="{{route('export.excel')}}" class="btn btn-success">Download Data Excel</a>
              <a href="{{route('employee.create')}}" class="btn btn-primary">Tambah Data Employee</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-dark">
                <thead>
                  <tr>
                    <td scope="col">ID</td>
                    <td scope="col">Nama</td>
                    <td scope="col">Atasan</td>
                    <td scope="col">Company</td>
                    <td scope="col">Aksi</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($employee as $c)
                    <tr>
                      <td scope="row">{{$c->id}}</td>
                      <td>{{$c->nama}}</td>
                      <td>{{$c->atasan_id == null ? 'Tidak ada' : $c->atasan->nama}}</td>
                      <td>{{$c->company_id == null ? 'Tidak ada' : $c->company->nama}}</td>
                      <td>
                        <a href="{{route('employee.edit', $c->id)}}" class="btn btn-warning btn-sm">Edit</a> | 
                        <a href="{{route('employee.delete', $c->id)}}" onclick="return confirm('Yakin ingin dihapus ?');" class="btn btn-danger btn-sm">Hapus</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection